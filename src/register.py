import os
import json
import boto3
import pytz
import datetime as dt

dynamodb_client = boto3.client('dynamodb')
dynamodb_table_logs = 'control-escolar-logs'
dynamodb_table_registers = 'control-escolar-registers'
zone_mx = pytz.timezone('America/Mexico_City')

def lambda_handler(event, context):
    status_code = 404
    body = json.loads(event['body'])
    id_student = body['id_student']
    id_register = body['id_register']
    timestamp = dt.datetime.now(zone_mx).strftime("%Y/%m/%d - %H:%M:%S")
    sort_date = dt.datetime.now(zone_mx).strftime("%Y/%m/%d")

    try:
        dynamodb_response_students = dynamodb_client.scan(
            TableName = dynamodb_table_registers,
            FilterExpression = "id_student = :id_student and sort_date = :sort_date",
            ExpressionAttributeValues = {
                ":id_student": {
                    "S": id_student
                },
                ":sort_date": {
                    "S": sort_date
                },
            }
        )
        if dynamodb_response_students['Items'] == []:
            update_to_entry(id_student, timestamp, sort_date)
            status_code = 201
        else:
            last_status = dynamodb_response_students['Items'][0]['last_status']['S']
            status_code = 200
            if last_status == "entry":
                update_to_exit(id_student, timestamp, sort_date)
            else:
                update_to_entry(id_student, timestamp, sort_date)
        register_log(timestamp, id_register, id_student)
    except Exception as e:
        status_code = 502
        print(e)
    
    return {
        "statusCode": status_code,
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    }

def update_to_entry(id_student, timestamp, sort_date):
    dynamodb_client.update_item(
        TableName = dynamodb_table_registers,
        Key={
            'id_student': {
                'S': id_student
            },
            'sort_date': {
                'S': sort_date
            }
        },
        AttributeUpdates = {
            'entry_timestamp' : {
                'Value':  {
                    "S": timestamp
                },
                'Action': 'PUT'
            },
            'last_status' : {
                'Value':  {
                    "S": "entry"
                },
                'Action': 'PUT'
            }
        }
    )

def update_to_exit(id_student, timestamp, sort_date):
    dynamodb_client.update_item(
        TableName = dynamodb_table_registers,
        Key={
            'id_student': {
                'S': id_student
            },
            'sort_date': {
                'S': sort_date
            }
        },
        AttributeUpdates = {
            'exit_timestamp' : {
                'Value':  {
                    "S": timestamp
                },
                'Action': 'PUT'
            },
            'last_status' : {
                'Value':  {
                    "S": "exit"
                },
                'Action': 'PUT'
            }
        }
    )

def register_log(timestamp, id_register, id_student):
    dynamodb_client.put_item(
        TableName = dynamodb_table_logs,
        Item = {
            "timestamp": {
                "S": timestamp
            },
            "id_register": {
                "S": id_register
            },
            "id_student": {
                "S": id_student
            }
        }
    )