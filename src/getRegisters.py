import os
import json
import boto3
import pytz
import datetime as dt

dynamodb_client = boto3.client('dynamodb')
dynamodb_table_registers = 'control-escolar-registers'
dynamodb_table_students = 'control-escolar-students'
zone_mx = pytz.timezone('America/Mexico_City')

def lambda_handler(event, context):
    status_code = 404
    entries = []
    exits = []
    today_date = dt.datetime.now(zone_mx).strftime("%Y/%m/%d")

    try:
        dynamodb_response_registers = dynamodb_client.scan(
            TableName = dynamodb_table_registers,
            FilterExpression = "sort_date = :today_date",
            ExpressionAttributeValues = {
                ":today_date": {
                    "S": today_date
                }
            }
        )
        file_list = dynamodb_response_registers['Items']
        for item in file_list:            
            id_student = item['id_student']['S']
            student_data = get_student_data(id_student)
            print(id_student, student_data)
            last_status = item['last_status']['S']
            if last_status == "entry":
                print("ENTRY:", id_student)
                entry_timestamp = item['entry_timestamp']['S']
                print("ENTRY:", id_student)
                json_response_entries = {"name_student": student_data['name_student'], "url_photo": student_data['url_photo'], "id_student":id_student, "timestamp": entry_timestamp, "id_grade": student_data['id_grade'], "id_group": student_data['id_group'], "id_matricula":student_data['id_matricula']}
                print("ENTRY:", id_student)
                entries.append(json_response_entries)
                entries.sort(key = get_timestamp)
            else:
                print("EXIT:", id_student)
                exit_timestamp = item['exit_timestamp']['S']
                json_response_exits = {"name_student": student_data['name_student'], "url_photo": student_data['url_photo'], "id_student":id_student, "timestamp": exit_timestamp, "id_grade": student_data['id_grade'], "id_group": student_data['id_group'], "id_matricula":student_data['id_matricula']}
                exits.append(json_response_exits)
                exits.sort(key = get_timestamp)
        status_code = 200
    except Exception as e:
        status_code = 204
        print("ERROR:", e)
    
    return {
        "statusCode": status_code,
        "body": json.dumps({
            "Entradas": entries,
            "Salidas": exits
        }),
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    }

def get_timestamp(register):
    return register.get('timestamp')

def get_student_data(id_student):
    json_response_student = {}
    dynamodb_response_students = dynamodb_client.scan(
        TableName = dynamodb_table_students,
        FilterExpression = "id_student = :id_student",
        ExpressionAttributeValues = {
            ":id_student": {
                "S": id_student
            }
        }
    )
    if dynamodb_response_students['Items'] == []:
        json_response_student = {"id_grade":"", "id_group": "", "id_matricula": "", "name_student": "GUEST - " + id_student, "url_photo": "https://public-bucket-images-beyond.s3.amazonaws.com/control-escolar/photos/Beyond_Technology.jpeg"}
    else:
        id_grade = dynamodb_response_students['Items'][0]['id_grade']['S']
        id_group = dynamodb_response_students['Items'][0]['id_group']['S']
        id_matricula = dynamodb_response_students['Items'][0]['id_matricula']['S']
        url_photo = dynamodb_response_students['Items'][0]['url_photo']['S']
        name_student = dynamodb_response_students['Items'][0]['name_student']['S']
        json_response_student = {"id_grade":id_grade, "id_group": id_group, "id_matricula": id_matricula, "name_student": name_student, "url_photo": url_photo}
    return json_response_student