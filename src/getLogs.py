import os
import json
import boto3

dynamodb_client = boto3.client('dynamodb')
dynamodb_table_logs = 'control-escolar-logs'

def lambda_handler(event, context):
    status_code = 404
    registers = []

    try:
        dynamodb_response = dynamodb_client.scan(
            TableName = dynamodb_table_logs
        )
        file_list = dynamodb_response['Items']
        for item in file_list:            
            id_student = item['id_student']['S']
            id_register = item['id_register']['S']
            timestamp  = item['timestamp']['S']
            json_response = {"id_student":id_student, "timestamp":timestamp, "id_register":id_register}
            registers.append(json_response)
        registers.sort(key = get_timestamp)
        status_code = 200
    except Exception as e:
        status_code = 204
        print(e)
    
    return {
        "statusCode": status_code,
        "body": json.dumps({
            "Registros": registers
        }),
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    }

def get_timestamp(register):
    return register.get('timestamp')